{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}  -- One more extension.
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}  -- To derive Show
{-# LANGUAGE TypeOperators      #-}

module Main where

import Relude
import Options.Generic
import qualified Data.Text as T
import System.Directory

import Sephibot

main :: IO ()
main = do
  config <- unwrapRecord "Sephibot"
  dbpath <- getDbPath config
  reportTime (unHelpful $ token config) dbpath

data Config w = Config
  { token :: Text <?> "Bot token"
  , dbpath :: Maybe FilePath <?> "DB path (default is ~/.local/share/sephibot/db.bin)"
  }
  deriving (Generic)

getDbPath :: Config w -> IO FilePath
getDbPath =
  maybe (getHomeDirectory <&> (<> "/.local/share/sephibot/db.bin")) pure . unHelpful . dbpath

instance ParseRecord (Config Wrapped)
deriving instance Show (Config Unwrapped)

