module When where

import Relude hiding (toLower, isPrefixOf)
import Relude.Unsafe as U (read, init, last)

import qualified Data.Text as T

import Data.Time
import Data.Fixed


sanitize :: Text -> Text
sanitize = T.dropWhile (==' ') . T.toLower

runWhenEnglish :: MonadIO m => UTCTime -> m Text
runWhenEnglish target = do
  now <- liftIO getCurrentTime
  pure $ printInterval mergeEnglish formatEnglish $ diffUTCTime target now

printInterval :: ([Text] -> Text) -> [Int -> Text] -> NominalDiffTime -> Text
printInterval merger formatting t =
  let
    units =
      fmap (U.read @Int . toString) $ words $ toText $ formatTime defaultTimeLocale "%w %D %H %M %S" t
  in
    merger $ filter (""/=) $ zipWith ($) formatting units

formatEnglish :: [Int -> Text]
formatEnglish =
  [ formatUnit "1 week" "2 weeks" "weeks"
  , formatUnit "1 day" "2 days" "days"
  , formatUnit "1 hour" "2 hours" "hours"
  , formatUnit "1 minute" "2 minutes" "minutes"
  , formatUnit "1 second" "2 seconds" "seconds"
  ]

mergeEnglish :: [Text] -> Text
mergeEnglish = \case
  [] -> "Yalla let's play!"
  [unit] -> unit <+> ". But who's counting?"
  units -> T.intercalate ", " (U.init units) <+> "and" <+> (U.last units) <+> ". But who's counting?"

runWhenHebrew :: MonadIO m => UTCTime -> m Text
runWhenHebrew target = do
  now <- liftIO getCurrentTime
  pure $ "עוד " <> printInterval mergeHebrew formatHebrew (diffUTCTime target now)


formatHebrew :: [Int -> Text]
formatHebrew =
  [ formatUnit "שבוע" "שבועיים" "שבועות"
  , formatUnit "יום" "יומיים" "ימים"
  , formatUnit "שעה" "שעתיים" "שעות"
  , formatUnit "דקה" "דקותיים" "דקות"
  , formatUnit "שנייה" "2 שניות" "שניות"
  ]

mergeHebrew :: [Text] -> Text
mergeHebrew = \case
  [] -> "יאללה בלאגן!"
  [unit] -> unit <+> ". אבל מי סופר?"
  units -> T.intercalate ", " (U.init units) <+> "ו" <> (U.last units) <+> ". אבל מי סופר?"



formatUnit :: Text -> Text -> Text -> Int -> Text
formatUnit singular doubler plural units
  | units > 2 = T.pack (show units) <+> plural
  | units == 2 = doubler
  | units == 1 = singular
  | otherwise = ""
  
(<+>) a b = a <> " " <> b

parseDatetime :: Text -> Either String UTCTime
parseDatetime =
  parseTimeM True defaultTimeLocale "%Y-%-m-%-d %H:%M %z" . T.unpack

