module Sephibot where

import Relude hiding (isPrefixOf)
import Control.Lens hiding (mapping)

import qualified Data.Text as T
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Concurrent

import Data.Time
import System.Random

import Discord
import Discord.Types
import qualified Discord.Requests as R

import When
import Types
import Logi
import SimpleFileDb

reportTime :: Text -> FilePath -> IO ()
reportTime token dbfile = do
  dbvar <- initializeDB dbfile defaultDB
  stdgenVar <- newTVarIO =<< getStdGen
  let
    mystate = MyState dbvar stdgenVar
  userFacingError <-
    runDiscord $ def
      { discordToken = token
      , discordOnEvent = eventHandler mystate
      , discordOnStart = do
        putTextLn "Bot is running."
      }
  void $ error userFacingError
  exitFailure

eventHandler :: MyState -> Event -> DiscordHandler ()
eventHandler mystate event =
  case event of
    MessageCreate m ->
      when (not (fromBot m)) $ handleMessage mystate m

    GuildCreate Guild{guildId} _ ->
      reminders guildId mystate

    _ -> do
      pure ()

fromBot :: Message -> Bool
fromBot m = userIsBot (messageAuthor m)

reactClock :: Message -> DiscordHandler ()
reactClock message =
  react message "alarm_clock"

reactCode :: Message -> DiscordHandler ()
reactCode message =
  react message "keyboard"

react :: Message -> Text -> DiscordHandler ()
react message =
  void . restCall . R.CreateReaction (messageChannel message, messageId message)

sendMsg :: Message -> Text -> DiscordHandler ()
sendMsg message =
  void . restCall . R.CreateMessage (messageChannel message)

whenToFormatter :: M.Map Text (UTCTime -> IO Text)
whenToFormatter = M.fromList
  [ ("when?", runWhenEnglish)
  , ("מתי?", runWhenHebrew)
  ]

helpMsg :: Text
helpMsg = unlines
  [ "xbox, ps3, 3ds nintendo wii - sephibot!"
  , ""
  , "Commands:"
  , "- `:set-date` - set date for a reminder"
  , "- `:scratcher <width> <height> <emote>` - create a scratcher puzzle"
  , "- `when?` - when is the reminder?"
  , "- `מתי?` - מתי is the reminder?"
  , ""
  , "To use logi, use a code block with the annotation `logi`."
  ]

handleMessage :: MyState -> Message -> DiscordHandler ()
handleMessage mystate message = do
  let
    txt = sanitize (messageText message)
  if
    | txt == ":help" -> do
      react message "eyes"
      sendMsg message helpMsg

    | "אתה " `T.isPrefixOf` txt && "?" `T.isSuffixOf` txt -> do
      let
        thing = (T.drop 4 . T.reverse . T.dropWhile (=='?') . T.reverse) txt
      if thing /= "ממוצע"
        then do
          react message "japanese_ogre"
          sendMsg message $ "לא " <> thing <> " ולא כלום"
        else do
          react message "angel"
          sendMsg message "כן ממוצע אני דווקא כן."

    | Just runWhen <- M.lookup txt whenToFormatter -> do
      reactClock message
      dates <- _targetDates <$> readDBIO (mystate ^. dbVar)
      case (`M.lookup` dates) =<< messageGuild message of
        Nothing ->
          sendMsg message "Date not set. Use the `:set-date` command to set a new date."
        Just (_, time) -> do
          sendMsg message =<< liftIO (runWhen time)

    | ":set-date" `T.isPrefixOf` txt -> do
      reactClock message
      case parseDatetime (T.drop (T.length ":set-date ") txt) of
        Left{} -> do
          sendMsg message $ unlines
            [ "Invalid date format."
            , "Example usage: `:set-date 2020-11-14 20:00 +0200`"
            ]
        Right time -> do
          now <- liftIO getCurrentTime
          if
            | time < now ->
              sendMsg message "Date already passed."

            | Just gid <- messageGuild message -> do
              atomically $ setDate mystate gid (Just (messageChannel message, time))
              sendMsg message ("New date is set to: " <> show time)

            | otherwise ->
              sendMsg message "Not in guild"

    | ":scratcher" `T.isPrefixOf` txt -> do
      react message "woman_detective"
      case T.words (T.drop (T.length ":scratcher ") txt) of
        [w, h, emote]
          | Just width <- readMaybe (T.unpack w)
          , Just height <- readMaybe (T.unpack h)
          -> do
            puzzle <- liftIO $ scratcher (width, height) emote
            sendMsg message ("```\n" <> puzzle <> "```\n")
        _ -> do
            sendMsg message $ unlines
              [ "Invalid input."
              , "Example usage: `:scratcher 4 6 :eyes:`"
              ]

    | Just code <- toLogi txt -> do
      reactCode message
      case messageGuild message of
        Nothing ->
          sendMsg message "Error: Not in a guild."
        Just gid -> do
          let
            username = userName (messageAuthor message)
          results <- runCode mystate gid (toString username) code
          case results of
            Nothing ->
              sendMsg message ("@" <> username <> " - Error: operation timed out.")
            Just output ->
              sendMsg message (unlines $ map toCodeBlock output)

    | otherwise ->
      pure ()

reminders :: GuildId -> MyState -> DiscordHandler ()
reminders gid mystate = do
  now <- liftIO getCurrentTime
  gidcids <- atomically $ do
    dates <- _targetDates <$> readDB (mystate ^. dbVar)

    let
      uniques = S.toList . S.fromList
      forMaybeNub mapping f = uniques $ mapMaybe f (M.toList mapping)

      gidcids = forMaybeNub dates $ \(gid', (cid, time)) ->
        if (gid == gid' && now >= time)
          then
            Just (gid, cid)
          else
            Nothing

    for_ gidcids $ \(gid', _) -> do
      setDate mystate gid' Nothing

    pure gidcids

  for_ gidcids $ \(gid', cid) -> do
    putTextLn $ "Sending reminder"
    void $ restCall $ R.CreateMessage cid ":game_die: @everyone prepare your dice and let's play! :game_die:"

  liftIO $ threadDelay 1000000
  reminders gid mystate

setDate :: MyState -> GuildId -> Maybe (ChannelId, UTCTime) -> STM ()
setDate mystate ids mtime =
  modifyDB (mystate ^. dbVar) $
    over targetDates $
      case mtime of
        Nothing ->
          M.delete ids
        Just time ->
          M.insert ids time

scratcher :: (Int, Int) -> Text -> IO Text
scratcher (subtract 1 -> width, subtract 1 -> height) emote = do
  x <- randomRIO (0, width)
  y <- randomRIO (0, height)
  let
    board = zipWith ($) (cycle [makeLine (width, height) (x, y) emote]) [0..height]
  pure $ T.unlines $ map T.unwords board

makeLine :: (Int, Int) -> (Int, Int) -> Text -> Int -> [Text]
makeLine (width, height) (sX, sY) spec linenum
  | sY == linenum =
    replicate sX normal <> [special spec] <> replicate (width - sX) normal
  | otherwise =
    replicate (width + 1) normal

normal = "||:snowflake:||"
special x = "||" <> x <> "||"

