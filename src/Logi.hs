{-# LANGUAGE NumericUnderscores #-}

module Logi
  ( toLogi
  , toCodeBlock
  , runCode
  )
where

import Relude hiding (isPrefixOf)
import Relude.Extra.Lens
import qualified Data.Text as T
import qualified Data.Map as M
import System.Timeout
import System.Mem
import Discord.Types
import System.IO.Unsafe (unsafePerformIO)

import Language.Logi
import Types
import SimpleFileDb

runCode :: MonadIO m => MyState -> GuildId -> FilePath -> Code -> m (Maybe [Code])
runCode mystate gid name (Code code) = do
  res <- liftIO $ timeout (2_000_000) $ atomically $ do
    db <- readDB (mystate ^. dbVar)
    randseed <- readTVar (mystate ^. randomSeed)
    let
      initialKnowledge = maybe defaultKnowledge id $ M.lookup gid (db ^. logiKnowledge)
    case unsafePerformIO $ execs randseed initialKnowledge name code of
      Left err ->
        pure [Code $ "Error: " <> ppErr err]
      Right (results, Ctx knowledge randseed') -> do
        modifyDB (mystate ^. dbVar) $ over logiKnowledge $ M.insert gid knowledge
        writeTVar (mystate ^. randomSeed) randseed'
        pure $ map (Code . ppToText . ppResults) results
  liftIO performGC
  pure res

toLogi :: Text -> Maybe Code
toLogi txt
  | logiBegin `T.isPrefixOf` txt
  , logiEnd   `T.isSuffixOf` txt
  = Just $ toCode txt

  | otherwise =
    Nothing
    

newtype Code = Code Text

logiBegin :: Text
logiBegin = "```logi\n"

logiEnd :: Text
logiEnd = "\n```"

toCode :: Text -> Code
toCode txt =
  ( Code
  . T.dropEnd (T.length logiEnd)
  . T.drop (T.length logiBegin)
  ) txt

toCodeBlock :: Code -> Text
toCodeBlock (Code txt) = "```\n" <> txt <> logiEnd
