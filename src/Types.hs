{-# language TemplateHaskell #-}

module Types where

import Relude hiding (put, get)
import Discord.Types
import qualified Data.Map as M
import Data.Serialize
import Control.Lens
import Control.Concurrent.STM (TBQueue)

import Data.Time
import System.Random
import Language.Logi
import SimpleFileDb

---
instance Serialize Day where
  put = put . toModifiedJulianDay
  get = ModifiedJulianDay <$> get
instance Serialize UTCTime where
  put time = put (utctDay time) *> put (utctDayTime time)
  get = UTCTime <$> get <*> get
instance Serialize DiffTime where
  put = put . diffTimeToPicoseconds
  get = picosecondsToDiffTime <$> get
instance Serialize Snowflake where
  put (Snowflake n) = put n
  get = Snowflake <$> get
---

data MyDB
  = MyDB
  { _targetDates :: M.Map GuildId (ChannelId, UTCTime)
  , _logiKnowledge :: M.Map GuildId Knowledge
  }
  deriving Generic

instance Serialize MyDB

data MyState
  = MyState
  { _dbVar :: DBVar MyDB
  , _randomSeed :: TVar StdGen
  }
  deriving Generic

makeLenses ''MyDB
makeLenses ''MyState

defaultDB :: MyDB
defaultDB = MyDB mempty mempty

data Config
  = Config
  { dbFilePath :: String
  }
